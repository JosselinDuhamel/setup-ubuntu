#!/bin/bash

if [ "$#" -ne 1 ]; then
    sudo -s ./install.sh $USER
    exit 1
fi

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

echo -e "\n---------------DEV FOLDER CREATED---------------\n"; sleep 1
if [[ -d '/home/joss/Dev' ]]
then
    echo "~/Dev exists in your home directory."
else
    mkdir /home/joss/Dev
    echo "Directory 'Dev' is created in your home directory"
fi

echo -e "\n---------------USER ADD TO SUDOERS---------------\n"; sleep 1

sudoers=`echo -e "$1\tALL=(ALL) NOPASSWD:ALL"`

if  cat /etc/sudoers | grep -q "$sudoers"
then
    echo -e "$1 already has privileges."
else
    echo -e "$1\tALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
    echo "Privileges set for $1"
fi

echo -e "\n---------------APT GET UPDATE---------------\n"; sleep 1
apt-get update

echo -e "\n---------------APT GET UPGRADE---------------\n"; sleep 1
apt-get upgrade -y

echo -e "\n---------------INSTALL EMACS---------------\n"; sleep 1
apt-get install -y emacs

echo -e "\n---------------INSTALL THUNAR---------------\n"; sleep 1
apt-get install -y thunar

echo -e "\n---------------INSTALL TREE---------------\n"; sleep 1
apt-get install -y tree

echo -e "\n---------------INSTALL PYTHON---------------\n"; sleep 1
apt-get install -y python

echo -e "\n---------------INSTALL PIP---------------\n"; sleep 1
apt-get install -y python3-pip

echo -e "\n---------------INSTALL VIRTUALBOX---------------\n"; sleep 1
apt-get install -y virtualbox

echo -e "\n---------------INSTALL VAGRANT---------------\n"; sleep 1
apt-get install -y vagrant

echo -e "\n---------------INSTALL GIT---------------\n"; sleep 1
apt-get install -y git

echo -e "\n---------------INSTALL SSH---------------\n"; sleep 1
apt-get install -y ssh

echo -e "\n---------------INSTALL GPARTED---------------\n"; sleep 1
apt-get install -y gparted

echo -e "\n---------------SET ALIAS IN BASHRC---------------\n"; sleep 1
if  cat ~/.bashrc | grep -q "alias bashrc='emacs ~/.bashrc'"
then
    echo "Aliases are already set."
else
    echo "alias emacs='emacs -nw'" >> ~/.bashrc
    echo "alias ls='ls --color=auto'" >> ~/.bashrc
    echo "alias bin='cd ~/bin'" >> ~/.bashrc
    echo "alias emacs='emacs -nw'" >> ~/.bashrc
    echo "alias ne='emacs'" >> ~/.bashrc
    echo "alias m='rm *~; rm \#*; rm .*~'" >> ~/.bashrc
    echo "alias ..='cd ..'" >> ~/.bashrc
    echo "alias dev='cd ~/Dev'" >> ~/.bashrc
    echo "alias grep='grep --color=auto'" >> ~/.bashrc
    echo "alias bye='shutdown -h now'" >> ~/.bashrc
    echo "alias bashrc='emacs ~/.bashrc'" >> ~/.bashrc    
    echo "Aliases set in your bashrc file."
fi

#installer docker
